package com.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javafx.util.Pair;

/**
 *
 * @author Dell
 */
public class PicasFijas {

    public Scanner scanner;

    public Integer preguntarNDigitos() {
        Integer longitudNumero = 0;
        scanner = new Scanner(System.in);
        Boolean ok = false;
        System.out.println("Ingrese un numero entre 4 y 7 para proceder a generar el numero aleatorio con la longitud que usted especifico");

        try {
            while (ok == false) {
                longitudNumero = scanner.nextInt();
                if (longitudNumero < 4 || longitudNumero > 7) {
                    System.out.println("Por favor ingrese en un rango entre 4 y 7");
                } else {
                    ok = true;
                }
            }

        } catch (Exception e) {
            System.out.println("No ingrese numeros con decimales ejemplo: 4.2 o 7.21 o 5,123 o caracteres que no sean del dominio de los NUMEROS ENTEROS!!");
        }
        return longitudNumero;

    }
    
    public Integer validarNumero(String numero)throws Exception{
    	if(!isNumeric(numero))throw new NumberFormatException("Ingresa �nicamente un n�mero entre 4 y 7");
      	if(Integer.parseInt(numero)<4 ||Integer.parseInt(numero)>7 )throw new Exception("El n�mero debe estar entre 4 y 7");
    	return Integer.parseInt(numero);
    }
    
    public  boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public boolean validarNumeroGenerado(int numeroGenerado) {
        boolean arreglo[] = new boolean[10];
        for (int i = 0; i < 10; i++) {
            arreglo[i] = false;
        }
        while (numeroGenerado > 0) {
            int digito = numeroGenerado % 10;
            if (arreglo[digito])
                return false;
            arreglo[digito] = true;
            numeroGenerado = numeroGenerado / 10;
        }
        return true;
    }

    public boolean validateIfLengthTheNumberIsFour(int numberGenerated) {
        String stringNumber = String.valueOf(numberGenerated);
        if (stringNumber.length() > 4 || stringNumber.length() < 4)
            return false;
        return true;
    }

    public Integer validateInputOnlyNumbers(String number) throws Exception {
        Integer num;
        try {
            num = Integer.parseInt(number);
            System.out.println(num + " is a valid float number");
        } catch (NumberFormatException e) {
            throw new Exception("Ingresa un numero de n cifras, donde n es algun numero entero "+e);
        }
        return num;
    }

    public Integer validateNumberLenghtEqualToGenerated(String numberInput, int numberGenerated) throws Exception {
        String numString = String.valueOf(numberGenerated);
        if(numberInput.length() != numString.length()) throw new Exception("Ingresa un numero de n cifras");
        return -1;
    }
    
    public int[] generarNumeroAleatorioEnArreglo(int numeroDigitos) {
        int arregloNumerosGenerados[] = new int[numeroDigitos];
        Random aleatorio = new Random(System.currentTimeMillis());
        for (int i = 0; i < numeroDigitos; i++) {
            int numero = aleatorio.nextInt(9);
            //System.out.println("numero generado " + numero + " iteracion " + i);
            arregloNumerosGenerados[i] = numero;
            for (int j = 0; j < i; j++) {
                if (arregloNumerosGenerados[i] == arregloNumerosGenerados[j]) {
                    i--;
                }
            }
        }
        return arregloNumerosGenerados;
    }

    public int generarNumeroEntero(int arreglo[]) {
        StringBuilder numeroGeneradoString = new StringBuilder();
        int numeroGenerado;
        for (int i = 0; i < arreglo.length; i++) {
            numeroGeneradoString.append(arreglo[i]);
        }
        numeroGenerado = Integer.parseInt(numeroGeneradoString.toString());
        return numeroGenerado;
    }
    
    public String cantidadPicasYFijas(int numeroGenerado, int numeroIngresado) {
    	int fijas =0;
    	int picas =0;
    	int [] picasFijas= new int[2];
    	int[] arrayNumGenerado = Integer.toString(numeroGenerado).chars().map(c -> c-'0').toArray();  
    	int[] arrayNumIngresado = Integer.toString(numeroIngresado).chars().map(c -> c-'0').toArray();  
    	for(int i =0;i <arrayNumGenerado.length;i++) {
    		if(arrayNumGenerado[i]==arrayNumIngresado[i])fijas++;
    		for(int u=0;u<arrayNumIngresado.length;u++) {
    			if(arrayNumGenerado[i]==arrayNumIngresado[u]&& i!=u)picas++;
    		}
    	}
    	
    	System.out.println(picas + " picas /  " + fijas + " fijas");
    	return picas+""+fijas;
    }
    
    public String finalizacionEnDiezIntentos(int numeroGenerado, int numeroIngresado, int intento) throws Exception{
    	String local = "";
    	int i = 0;
    	int nFijas = 0;
    	if(intento == 0) throw new Exception("El intento no puede ser cero");
    	if(intento == 10) throw new Exception("Perdiste el numero maximo de intentos es diez");
    	while(i < 10) {
    		local = cantidadPicasYFijas(numeroGenerado, numeroIngresado);
    		String nFijasString = String.valueOf(local.charAt(1));
    		nFijas = Integer.parseInt(nFijasString);
    		if(nFijas == 4 && intento < (i+1)) {
    			System.out.println("Ganaste, tienes: "+nFijas+" fijas");
    			break;
    		}
    		i++;
    	}
    	return String.valueOf(nFijas);
    	
    }
    
    public void preguntarNumeroAdivinar() {
        scanner = new Scanner(System.in);
        System.out.println("Ingrese un numero para mirar si lo adivina :)");
        int repeticiones[] = new int[10];
        StringBuilder builder = new StringBuilder();
        try {
            int numero = scanner.nextInt();

            String stringNumero = String.valueOf(numero);
            int arregloNumeroIngresado[] = new int[stringNumero.length()];
            for (int i = 0; i < stringNumero.length(); i++) {
                String local = String.valueOf(stringNumero.charAt(i));
                int localInt = Integer.parseInt(local);
                arregloNumeroIngresado[i] = localInt;

            }

            for (int j = 0; j < arregloNumeroIngresado.length; j++) {
                repeticiones[arregloNumeroIngresado[j]] += 1;
            }

            for (int k = 0; k < repeticiones.length; k++) {
                if (repeticiones[k] > 1) {
                    String local = "el numero " + k + " se repite " + repeticiones[k] + " veces, ";
                    builder.append(local);
                }
            }
            builder.append("por favor reingresa el numero sin repetir la secuencia de numeros");
            System.out.println(builder);

        } catch (Exception e) {
            System.out.println("No ingrese numeros con decimales ejemplo: 4.2 o 7.21 o 5,123 o caracteres que no sean del dominio de los NUMEROS ENTEROS!!");
        }
    }

    public static void main(String[] args) throws Exception {
        PicasFijas picasFijas = new PicasFijas();
        //int cantidad = picasFijas.preguntarNDigitos();
        //int[] arreglo = new int[cantidad];
        //arreglo = picasFijas.generarNumeroAleatorioEnArreglo(cantidad);
        //System.out.println("numero generado: " + picasFijas.generarNumeroEntero(arreglo));
        //picasFijas.preguntarNumeroAdivinar();
        //picasFijas.comprobarRepeticiones();
        //picasFijas.finalizacionEnDiezIntentos(1256, 1256, 9);
    }
}
