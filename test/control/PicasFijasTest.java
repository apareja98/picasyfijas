package control;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.control.PicasFijas;

class PicasFijasTest {
	PicasFijas picasFijas= new PicasFijas();
	
	@Test
	void aTest() {
		assertThrows(NumberFormatException.class, ()->picasFijas.validarNumero("b7"));
	}
	
	@Test
	void bTest() {
		assertThrows(NumberFormatException.class, ()->picasFijas.validarNumero(""));
	}
	@Test
	void cTest() {
		assertThrows(Exception.class, ()->picasFijas.validarNumero("3"));
	}
	@Test
	void dTest() {
		assertThrows(Exception.class, ()->picasFijas.validarNumero("10"));
	}
	@Test
    @DisplayName("Verificar si el numero generado contiene numeros repetivos")
    void eTest() throws Exception {
        assertTrue(()->picasFijas.validarNumeroGenerado(1234), "El numero contiene digitos repetivos");
    }

    @Test
    @DisplayName("Verificar si el numero generado tiene una longitud de cuatro caracteres")
    void fTest() {
        assertTrue(()->picasFijas.validateIfLengthTheNumberIsFour(2364), "El numero generado no contiene el minimo y maximo de caracteres el cual son cuatro");
    }

    @Test
    @DisplayName("Verificar si solo se ingreso numeros")
    void gTest() {
        assertThrows(Exception.class, ()->picasFijas.validateInputOnlyNumbers("1234h"));
    }
    @Test
    void hTest() {
        assertThrows(Exception.class, ()->picasFijas.validateNumberLenghtEqualToGenerated("12345", 1234));
    }
	@Test
	void iTest() {
		assertTrue(picasFijas.validarNumeroGenerado(1202));
		
	}
	@Test
	void jTest() {
		assertEquals(picasFijas.cantidadPicasYFijas(1425, 1452),"22");
	}
	@Test
	void kTest() throws Exception {
		assertEquals(picasFijas.finalizacionEnDiezIntentos(3478, 3478, 10), "4");
	}
	@Test
	void lTest() {
		
	}
	
}
